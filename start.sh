#!/usr/bin/env bash

docker run \
    --rm \
    --name=proxy-conf \
    --hostname=proxy-conf.test.accomodata.net \
    -it \
    -e "WILDCARD_CERTS=*.test.accomodata.net,*.dockertest01.apertoso.net" \
    -v "/var/run/docker.sock:/var/run/docker.sock" \
    -v "/srv/caddy/:/srv/caddy" \
    -v "/opt/proxy-conf/caddy_proxy_conf.py:/caddy_proxy_conf.py" \
    -v "/opt/proxy-conf/templates:/templates" \
    registry.gitlab.com/apertoso/docker/nginx-certbot-conf \
    python /caddy_proxy_conf.py